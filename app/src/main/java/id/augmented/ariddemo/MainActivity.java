package id.augmented.ariddemo;

import android.Manifest;
import android.content.Intent;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.github.ybq.android.spinkit.SpinKitView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    FloatingActionsMenu actionsMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imgV = (ImageView) findViewById(R.id.main_background_image);
        Picasso.with(this).load(R.drawable.bg_cyborg).fit().into(imgV);
        FloatingActionButton vr = (FloatingActionButton) findViewById(R.id.vr_button);
        vr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openVR();
            }
        });
        FloatingActionButton ar = (FloatingActionButton) findViewById(R.id.ar_button);
        ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAR();
            }
        });

        Dexter.checkPermission(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {

            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {

            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

            }
        }, Manifest.permission.CAMERA);
    }

    public void openVR() {
        Intent intent = new Intent(this, VRActivity.class);
        startActivity(intent);

    }

    public void openAR() {
        SpinKitView spv = (SpinKitView) findViewById(R.id.spin_kit);
        spv.setVisibility(View.GONE);
        spv = null;
        Intent intent = new Intent(this, ARActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("Native Lib",this.getApplicationInfo().nativeLibraryDir);
    }
}
