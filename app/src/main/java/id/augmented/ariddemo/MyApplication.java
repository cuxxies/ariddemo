package id.augmented.ariddemo;

import android.app.Application;

import com.karumi.dexter.Dexter;
import com.splunk.mint.Mint;

/**
 * Created by hendri on 10/30/16.
 */
public class MyApplication extends Application {
    public boolean isInitial = false;

    @Override
    public void onCreate() {
        super.onCreate();
        Dexter.initialize(getApplicationContext());
        Mint.initAndStartSession(getApplicationContext(), "d9287bd3");
        // Mint.initAndStartSession(getApplicationContext(), "b5c4aeda");

    }
}