package id.augmented.ariddemo;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.vr.sdk.widgets.common.VrWidgetView;
import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import id.augmented.ariddemo.CustomClasses.WABottomSheetBehavior;

public class VRActivity extends AppCompatActivity{

    private static final String TAG = "VRActivity";
    VrPanoramaView panoramaView;
    boolean loadImageSuccessful;
    private ImageLoaderTask backgroundImageLoaderTask;
    private VrPanoramaView.Options panoOptions = new VrPanoramaView.Options();
    private ImageButton menuButton;
    private WABottomSheetBehavior mBottomSheetBehav;
    private ArrayList<String> mainMenuArray;
    private ArrayList<Pair<String,String>> subMenuArray;
    private ArrayAdapter<String> mainMenuAdapter;
    private ArrayAdapter<String> subMenuAdapter;
    private ListView mainMenuView;
    private ListView subMenuView;
    private TextView headingText;
    private int bottomMenuState = 0;

    private ArrayList<String> buildNameArray(ArrayList<Pair<String,String>> inp)
    {
        ArrayList<String> out = new ArrayList<String>();
        for(int i=0;i<inp.size();i++){
            Pair<String,String> pair = inp.get(i);
            out.add(pair.first);
        }
        return out;
    }

    private void buildMenuArrayInitial()
    {
        mainMenuArray = new ArrayList<String>();
        subMenuArray = new ArrayList<Pair<String,String>>();

        mainMenuArray.add("Augmented ID");


        subMenuArray.add(new Pair<String, String>("Living Room","ariddemo_livingroom_2.jpg"));
        subMenuArray.add(new Pair<String, String>("Master Bedroom ","ariddemo_masterbed_2.jpg"));
        subMenuArray.add(new Pair<String, String>("Master Bathroom","ariddemo_masterbath_2.jpg"));
        subMenuArray.add(new Pair<String, String>("2nd Bedroom ","ariddemo_bedroom2_2.jpg"));
        subMenuArray.add(new Pair<String, String>("3rd Bedroom","ariddemo_bedroom3_2.jpg"));

/*
        subMenuArray.add(new Pair<String, String>("Living Room","ariddemo_livingroom.jpg"));
        subMenuArray.add(new Pair<String, String>("Master Bedroom ","ariddemo_bedroom3.jpg"));
        subMenuArray.add(new Pair<String, String>("Bathroom","ariddemo_bathroom.jpg"));
        subMenuArray.add(new Pair<String, String>("2nd Bedroom ","ariddemo_bedroom2.jpg"));
        subMenuArray.add(new Pair<String, String>("3rd Bedroom","ariddemo_bedroom.jpg")); */
        mainMenuAdapter = new ArrayAdapter<String>(this,R.layout.list_main_menu_item,mainMenuArray);
        subMenuAdapter = new ArrayAdapter<String>(this,R.layout.list_main_menu_item,buildNameArray(subMenuArray));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vr);
        headingText = (TextView)findViewById(R.id.title_text);
        panoramaView = (VrPanoramaView)findViewById(R.id.pano_view);
        panoramaView.setEventListener(new ActivityEventListener());
//        panoramaView.setFullscreenButtonEnabled(false);
        panoramaView.setInfoButtonEnabled(false);
        //VrWidgetView vrWidgetView;
        // panoramaView
//        panoramaView.setStereoModeButtonEnabled(false);
       // panoramaView.setClickable(true);

        panoOptions.inputType = VrPanoramaView.Options.TYPE_STEREO_OVER_UNDER;
        menuButton = (ImageButton) findViewById(R.id.menu_bottom_button);

       // panoOptions.
        load360Image("ariddemo_livingroom_2.jpg",1);

       // load360Image("andes.jpg",2);
        //panoramaView
        RelativeLayout bottomSheetLayout
                = (RelativeLayout) findViewById(R.id.linear_layout_bottom_sheet);

        mainMenuView = (ListView)findViewById(R.id.menu_list_main);
        subMenuView = (ListView)findViewById(R.id.menu_list_sub);
        subMenuView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectSubMenu(view,i);
            }
        });
        buildMenuArrayInitial();
        headingText.setText(subMenuArray.get(0).first);
        mainMenuView.setAdapter(mainMenuAdapter);
        subMenuView.setAdapter(subMenuAdapter);


//get bottom sheet behavior from bottom sheet view
        mBottomSheetBehav = (WABottomSheetBehavior) WABottomSheetBehavior.from(bottomSheetLayout);
        mBottomSheetBehav.setAllowUserDragging(false);
        Resources r = getResources();
        int px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, r.getDisplayMetrics());
        mBottomSheetBehav.setPeekHeight(px);
        mBottomSheetBehav.setHideable(false);
       // mBottomSheetBehav.set

       /* mBottomSheetBehav.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                   mBottomSheetBehav.setState(bottomMenuState);
                    //mBottomSheetBehav.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                else
                {
                    bottomMenuState = newState;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        }); */

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBottomSheetBehav.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    mBottomSheetBehav.setState(BottomSheetBehavior.STATE_EXPANDED);
                else if(mBottomSheetBehav.getState() == BottomSheetBehavior.STATE_EXPANDED)
                    mBottomSheetBehav.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }
        });
    }

    @Override
    protected void onPause() {
        if(loadImageSuccessful)
            panoramaView.pauseRendering();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(loadImageSuccessful)
            panoramaView.resumeRendering();
    }

    @Override
    protected void onDestroy() {
        // Destroy the widget and free memory.
        panoramaView.shutdown();

        // The background task has a 5 second timeout so it can potentially stay alive for 5 seconds
        // after the activity is destroyed unless it is explicitly cancelled.
        if (backgroundImageLoaderTask != null) {
            backgroundImageLoaderTask.cancel(true);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (0 == getSupportFragmentManager().getBackStackEntryCount()) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void showBottomBarPeek()
    {
        mBottomSheetBehav.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    private void load360Image(String filename, int selectedOptions)
    {
        // Load the bitmap in a background thread to avoid blocking the UI thread. This operation can
        // take 100s of milliseconds.
        if (backgroundImageLoaderTask != null) {
            // Cancel any task from a previous intent sent to this activity.
            backgroundImageLoaderTask.cancel(true);
        }
        loadImageSuccessful = false;
        backgroundImageLoaderTask = new ImageLoaderTask();
        //panoOptions.inputType = selectedOptions;
        if(selectedOptions == 2)
            panoOptions.inputType = VrPanoramaView.Options.TYPE_STEREO_OVER_UNDER;
        else
            panoOptions.inputType = VrPanoramaView.Options.TYPE_MONO;
        backgroundImageLoaderTask.execute(Pair.create(filename, panoOptions));
    }

    void selectSubMenu(View view, int index)
    {
        String filename = subMenuArray.get(index).second;
        headingText.setText(subMenuArray.get(index).first);
        if(filename.equals("andes.jpg"))
           load360Image(filename,2);
        else
            load360Image(filename,1);
    }

    /**
     * Helper class to manage threading.
     */
    class ImageLoaderTask extends AsyncTask<Pair<String, VrPanoramaView.Options>, Void, Boolean> {

        /**
         * Reads the bitmap from disk in the background and waits until it's loaded by pano widget.
         */
        @Override
        protected Boolean doInBackground(Pair<String, VrPanoramaView.Options>... fileInformation) {
            VrPanoramaView.Options panoOptions = null;  // It's safe to use null VrPanoramaView.Options.
            InputStream istr = null;
            AssetManager assetManager = getAssets();
            if (fileInformation == null || fileInformation.length < 1
                    || fileInformation[0] == null || fileInformation[0].first == null || fileInformation[0].first.length() == 0) {

                try {
                    istr = assetManager.open("andes.jpg");
                    panoOptions = new VrPanoramaView.Options();
                    panoOptions.inputType = VrPanoramaView.Options.TYPE_STEREO_OVER_UNDER;
                } catch (IOException e) {
                    Log.e(TAG, "Could not decode default bitmap: " + e);
                    return false;
                }
            } else {
                try {
                   // istr = new FileInputStream(new File(fileInformation[0].first.getPath()));
                    istr = assetManager.open(fileInformation[0].first);
                    panoOptions = fileInformation[0].second;
                } catch (IOException e) {
                    Log.e(TAG, "Could not load file: " + e);
                    return false;
                }
            }

            panoramaView.loadImageFromBitmap(BitmapFactory.decodeStream(istr), panoOptions);
            try {
                istr.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close input stream: " + e);
            }

            return true;
        }
    }

    /**
     * Listen to the important events from widget.
     */
    private class ActivityEventListener extends VrPanoramaEventListener {
        /**
         * Called by pano widget on the UI thread when it's done loading the image.
         */
        @Override
        public void onLoadSuccess() {
            loadImageSuccessful = true;
            mBottomSheetBehav.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        /**
         * Called by pano widget on the UI thread on any asynchronous error.
         */
        @Override
        public void onLoadError(String errorMessage) {
            loadImageSuccessful = false;
            Toast.makeText(
                    VRActivity.this, "Error loading pano: " + errorMessage, Toast.LENGTH_LONG)
                    .show();
            Log.e(TAG, "Error loading pano: " + errorMessage);
        }
    }
}
