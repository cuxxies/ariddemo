package id.augmented.ariddemo;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.SpinKitView;

import com.github.ybq.android.spinkit.style.CubeGrid;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.github.ybq.android.spinkit.style.MultiplePulseRing;
import com.vuforia.Vuforia;
import com.vuforia.CameraDevice;
import com.vuforia.DataSet;

import com.vuforia.ObjectTracker;
import com.vuforia.STORAGE_TYPE;
import com.vuforia.State;
import com.vuforia.Trackable;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;

import id.augmented.ariddemo.Vuforia.ImageTargetRenderer;
import id.augmented.ariddemo.Vuforia.SampleApplicationGLView;
import id.augmented.ariddemo.Vuforia.VuforiaExeption;
import id.augmented.ariddemo.Vuforia.VuforiaInterface;
import id.augmented.ariddemo.Vuforia.VuforiaSession;

public class ARActivity extends AppCompatActivity implements VuforiaInterface {
    String LOGTAG = "TESTAR-Activity";
    VuforiaSession vuforiaAppSession;
    private DataSet mCurrentDataset;
    SampleApplicationGLView mGlView;
    ImageTargetRenderer mRenderer;
    SpinKitView spinKitView;
    FrameLayout loaderContainer;
    public boolean isShowingSpinner = true;
//    String strDatasetLoc = "real-living.xml";
    String strDatasetLoc = "ariddemo.xml";
    boolean mIsExtendedTracking;
    FrameLayout mUILayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ar);
      //  loaderContainer = (FrameLayout)findViewById(R.id.loader_container);
       // spinKitView = (SpinKitView)findViewById(R.id.spin_kit_ar);
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        mUILayout = (FrameLayout) findViewById(R.id.augmented_layout);
        showLoader();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (vuforiaAppSession == null) {
            vuforiaAppSession = new VuforiaSession(this);
            vuforiaAppSession
                    .initAR(this, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        }
    }

    // Initializes AR application components.
    private void initApplicationAR() {
        // Create OpenGL ES view:

        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        mGlView = new SampleApplicationGLView(this);
       /* mGlView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(mRenderer.getVideoRender().isTapOnScreenInsideTarget(0,event.getX(),event.getY()))
                    mRenderer.getVideoRender().playVideoFromTap();
                return false;
            }
        }); */
        mGlView.init(translucent, depthSize, stencilSize);

        mRenderer = new ImageTargetRenderer(this, vuforiaAppSession);
        //mRenderer.setTextures(mTextures);
        mGlView.setRenderer(mRenderer);

    }

    public boolean isExtendedTrackingActive() {
        return mIsExtendedTracking;
    }

    @Override
    public boolean doInitTrackers() {
        // Indicate if the trackers were initialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        // Trying to initialize the image tracker
        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null) {
            Log.e(
                    LOGTAG,
                    "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else {
            Log.i(LOGTAG, "Tracker successfully initialized");
        }
        return result;
    }

    @Override
    public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset == null)
            mCurrentDataset = objectTracker.createDataSet();

        if (mCurrentDataset == null)
            return false;

        if (!mCurrentDataset.load(
                strDatasetLoc,
                STORAGE_TYPE.STORAGE_APPRESOURCE))
            return false;

        if (!objectTracker.activateDataSet(mCurrentDataset))
            return false;

        int numTrackables = mCurrentDataset.getNumTrackables();
        for (int count = 0; count < numTrackables; count++) {
            Trackable trackable = mCurrentDataset.getTrackable(count);
            if (isExtendedTrackingActive()) {
                trackable.startExtendedTracking();
            }

            String name = "Current Dataset : " + trackable.getName();
            trackable.setUserData(name);
            Log.d(LOGTAG, "UserData:Set the following user data "
                    + (String) trackable.getUserData());
        }

        return true;
    }

    @Override
    public boolean doUnloadTrackersData() {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset != null && mCurrentDataset.isActive()) {
            if (objectTracker.getActiveDataSet().equals(mCurrentDataset)
                    && !objectTracker.deactivateDataSet(mCurrentDataset)) {
                result = false;
            } else if (!objectTracker.destroyDataSet(mCurrentDataset)) {
                result = false;
            }

            mCurrentDataset = null;
        }

        return result;
    }

    @Override
    public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();

        return result;
    }

    @Override
    public boolean doStopTrackers() {
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();

        return result;
    }


    @Override
    public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());

        return result;
    }

    @Override
    public void onInitARDone(VuforiaExeption e) {
        if (e == null) {
            initApplicationAR();

            mRenderer.mIsActive = true;

            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            //  int val = (int)getResources().getDimension(R.dimen.nav_left_margin);
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            lp.setMargins(0, 0, 0, 0);
           // mUILayout.addView(mGlView,lp);
            mUILayout.addView(mGlView, 0, lp);
            hideLoader();
            //   hideLoader();
            // Sets the UILayout to be drawn in front of the camera
            //  mUILayout.bringToFront();

            // Sets the layout background to transparent
            mUILayout.setBackgroundColor(Color.TRANSPARENT);

            try {
                vuforiaAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT);
            } catch (VuforiaExeption vuforiaExeption) {
                Log.e("", vuforiaExeption.getString());
            }

            boolean result = CameraDevice.getInstance().setFocusMode(
                    CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);

           /* if (result)
                mContAutofocus = true;
            else
                Log.e("", "Unable to enable continuous autofocus"); */

          /*  mSampleAppMenu = new SampleAppMenu(this, this, "Image Targets",
                    mGlView, mUILayout, null);
            setSampleAppMenuSettings(); */

        } else {
            Log.e("", e.getString());
            //  showInitializationErrorMessage(exception.getString());
        }
    }

    @Override
    public void onVuforiaUpdate(State state) {
       /* if (mSwitchDatasetAsap)
        {
            mSwitchDatasetAsap = false;
            TrackerManager tm = TrackerManager.getInstance();
            ObjectTracker ot = (ObjectTracker) tm.getTracker(ObjectTracker
                    .getClassType());
            if (ot == null || mCurrentDataset == null
                    || ot.getActiveDataSet() == null)
            {
                Log.d(LOGTAG, "Failed to swap datasets");
                return;
            }

            doUnloadTrackersData();
            doLoadTrackersData();
        } */
    }

    @Override
    public void onResume() {
        super.onResume();
        // This is needed for some Droid devices to force portrait


        try
        {
            vuforiaAppSession.resumeAR();
        } catch (VuforiaExeption e)
        {
            Log.e(LOGTAG, e.getString());
        }

        // Resume the GL view:
        if (mGlView != null)
        {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }
    }

    @Override
    public void onPause() {
        Log.d(LOGTAG, "onPause");
        super.onPause();

        if (mGlView != null)
        {
            mGlView.setVisibility(View.INVISIBLE);
            mGlView.onPause();
        }

        try
        {
            vuforiaAppSession.pauseAR();

        } catch (VuforiaExeption e)
        {
            Log.e(LOGTAG, e.getString());
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOGTAG, "onDestroy");
        try
        {
            vuforiaAppSession.stopAR();
            mRenderer = null;
        } catch (VuforiaExeption e)
        {
            Log.e(LOGTAG, e.getString());
        }
        System.gc();
    }

    public void showLoader()
    {
        ProgressBar progressBar = new ProgressBar(this);//(ProgressBar)findViewById(R.id.progress);
        MultiplePulseRing doubleBounce = new MultiplePulseRing();
        progressBar.setIndeterminate(true);
        progressBar.setIndeterminateDrawable(doubleBounce);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER;
        progressBar.setLayoutParams(lp);
    /*    Log.i("Show","Loader");
        SpinKitView spinKitView = new SpinKitView(this);

        spinKitView.setLayoutParams(lp);
        spinKitView.*/
        mUILayout.addView(progressBar);
       // loaderContainer.setVisibility(View.VISIBLE);
        isShowingSpinner = true;
    }

    public void hideLoader()
    {
        Log.i("Hide","Loader");
       // loaderContainer.setVisibility(View.INVISIBLE);
        int i = mUILayout.getChildCount();
        if(i>1)
            mUILayout.removeViewAt(i-1);
        isShowingSpinner = false;
    }


}