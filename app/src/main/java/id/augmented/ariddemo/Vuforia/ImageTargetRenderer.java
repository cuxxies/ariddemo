/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package id.augmented.ariddemo.Vuforia;


import android.app.Activity;
import android.content.res.AssetManager;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;

import com.threed.jpct.Camera;
import com.threed.jpct.Config;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Light;
import com.threed.jpct.Loader;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.MemoryHelper;
import com.vuforia.CameraCalibration;
import com.vuforia.Matrix44F;
import com.vuforia.Renderer;
import com.vuforia.State;
import com.vuforia.Tool;
import com.vuforia.Trackable;
import com.vuforia.TrackableResult;
import com.vuforia.Vec2F;
import com.vuforia.Vuforia;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Set;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import id.augmented.ariddemo.ARActivity;


// The renderer class for the ImageTargets sample.
public class ImageTargetRenderer implements GLSurfaceView.Renderer {
    private static final String LOGTAG = "ImageTargetRenderer";
    private VuforiaSession vuforiaAppSession;
    private ARActivity mActivity;
    public boolean isPlayingVideo = false;
    private Renderer mRenderer;
    public boolean mIsActive = false;
    public boolean mIsInitial = true;
    private World world;
    private Light sun;
    private Object3D[] object3Ds;
    private Object3D cylinder;
    private Camera cam;
    private FrameBuffer fb;
    private float[] modelViewMat;
    private float fov;
    private float fovy;
    private boolean isLoadingModel = false;
    String trackerStr = "";
    JSONObject jsonAR;
    JSONArray targetJSONArray;
    ArrayList<String> targetArray;
    ArrayList<String> objectArray;
    Object3D objMerged;
    int iFr = 0;
   // ARActivity arActivity;

    public ImageTargetRenderer(ARActivity activity, VuforiaSession session) {



        mActivity = activity;
     //   arActivity = (ARActivity)activity;

        vuforiaAppSession = session;
        // jsonAR = jsonObject;

        objectArray = new ArrayList<String>();
        //assignJSONObect();
        world = new World();
        world.setAmbientLight(150, 150, 150);
        // set the following value according to your need, so the object won't be disappeared.
        // world.setClippingPlanes(2.0f, 3000.0f);
        world.setClippingPlanes(10.0f, 50000.0f);

        sun = new Light(world);
        sun.setIntensity(250, 250, 250);

        // Create a texture out of the icon...:-)
      /*  if ( !TextureManager.getInstance().containsTexture("texture") ) {
            Texture texture = new Texture(BitmapHelper.rescale(BitmapHelper.convert(
                    mActivity.getResources().getDrawable(R.drawable.cascadelogostd)), 64, 64));
            TextureManager.getInstance().addTexture("texture", texture);
        }*/

        //  for(Object3D obj : object3Ds)
        //  {
        //
        //  }
        // Create a texture out of the icon...:-)
    /*    if ( !TextureManager.getInstance().containsTexture("texture") ) {
            Texture texture = new Texture(BitmapHelper.rescale(BitmapHelper.convert(
                    mActivity.getResources().getDrawable(R.drawable.cascadelogostd)), 64, 64));
            TextureManager.getInstance().addTexture("texture", texture);
        }

        cylinder = Primitives.getCylinder(20, 40);
        cylinder.calcTextureWrapSpherical();
        cylinder.setTexture("texture");
        cylinder.strip();
        cylinder.build(); */

        // Transform (scale, rotate, translate) the object: Depends on your need.
//    	cylinder.scale(scale);
        //    cylinder.rotateX(90.0f);
//    	cylinder.rotateY(w); cylinder.rotateZ(w);
//    	cylinder.translate(x, y, z);
        //    world.addObjects(object3Ds);
        //  world.addObject(cylinder);
        //  world.buildAllObjects();

        cam = world.getCamera();

        SimpleVector sv = new SimpleVector();
        // sv.y = 100;
        // sv.z += 100;
        sun.setPosition(sv);

        // for older Android versions, which had massive problems with garbage collection
        MemoryHelper.compact();

    }
        private void showLoader() {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mActivity.showLoader();
                }
            });
        }
    private void hideLoader() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                    mActivity.hideLoader();
                // caller.hideLoader();
            }
        });
    }

 /*   private void assignJSONObect() {
        targetArray = new ArrayList<String>();
        try {
            targetJSONArray = jsonAR.getJSONArray("Targets");
            for (int i = 0; i < targetJSONArray.length(); i++) {
                targetArray.add(targetJSONArray.getJSONObject(i).getString("Name"));
            }
        } catch (JSONException ex) {
            targetJSONArray = new JSONArray();
            ex.printStackTrace();
        }
    }
    */

    private void loadSelectedObjectFiles(String name) {

    }


    private void loadObjectToWorld(String currentTracker) {
        if (trackerStr.contains(currentTracker)) {
            return;
        }
        if (isLoadingModel) // if working on loading, do not load another one
        {
            Log.i("ImageTargetRender", "3D Model is still being loaded");
            return;
        }
        trackerStr = currentTracker;


        loadObjectToWorldAsync(trackerStr);
    }
/*
    public static SimpleVector getSize(Object3D object) {
        float[] bbox = object.getMesh().getBoundingBox();
        float s = object.getScale();
        Object3D[] par;
        par = object.getParents();
        while (par.length > 0) {
            s = s * par[0].getScale();
            par = par[0].getParents();
        }
        SimpleVector out = new SimpleVector((bbox[1] - bbox[0]) * s, (bbox[3] - bbox[2]) * s, (bbox[5] - bbox[4]) * s);
        return out;
    } */

    void loadObjectToWorldSync(final String currentTracker) {
/*        loadSelectedObjectFiles(currentTracker);
        if (objectArray.size() < 1)
            return;*/
        objMerged = null;
        //  AssetManager assetManager = mActivity.getAssets();
        // ;
        try {
            //world.removeAllObjects();
            //world.getObjects().


            for (Enumeration<Object3D> e = world.getObjects(); e.hasMoreElements();)
                e.nextElement().clearObject();
            world.removeAllObjects();

          /*  File objFile = getObjWithExtension(".obj");
            if (objFile == null)
                return;

            File mtlFile = getObjWithExtension(".mtl");
            if (mtlFile == null)
                return; */

            TextureManager tm = TextureManager.getInstance();
            tm.flush();

            //  FileInputStream objIS =  new FileInputStream(objFile);
            //   FileInputStream mtlIS = new FileInputStream(mtlFile);
            AssetManager assetManager = mActivity.getAssets();
            String obj = "";
            String mtl = "";

/*            <ImageTarget name="building" size="10.000000 4.263862" />
            <ImageTarget name="cascade" size="10.000000 5.296296" />
            <ImageTarget name="floorplan_4" size="10.000000 20.443213" />
            <ImageTarget name="floorplan_3" size="10.000000 20.409357" />
            <ImageTarget name="floorplan_2" size="10.000000 20.710060" />
            <ImageTarget name="floorplan_1" size="10.000000 19.005526" />*/


/*
            if(currentTracker.equalsIgnoreCase("building"))
            {
                obj = "unit2.obj";
                mtl = "unit2.mtl";
            }
            else if(currentTracker.equalsIgnoreCase("cascade"))
            {
                return;
            }
            else if(currentTracker.equalsIgnoreCase("floorplan_4"))
            {
                obj = "Floor04.obj";
                mtl = "Floor04.mtl";
            }
            else if(currentTracker.equalsIgnoreCase("floorplan_3"))
            {
                obj = "Floor03.obj";
                mtl = "Floor03.mtl";
            }
            else if(currentTracker.equalsIgnoreCase("floorplan_2"))
            {
                obj = "Floor02.obj";
                mtl = "Floor02.mtl";
            }
            else if(currentTracker.equalsIgnoreCase("floorplan_1"))
            {
                obj = "archilogic.obj";
                mtl = "archilogic.mtl";
            }
            */

            if(currentTracker.equalsIgnoreCase("pohan") || currentTracker.equalsIgnoreCase("hendri") ||
                    currentTracker.equalsIgnoreCase("siska") || currentTracker.equalsIgnoreCase("shiela") ||
                    currentTracker.equalsIgnoreCase("andi") || currentTracker.equalsIgnoreCase("reynald") ) {
                obj = "coba.obj";
                mtl = "coba.mtl";
            }
            else
            {
               // hideLoader();
                return;

            }
          //  object3Ds = Loader.load3DS(assetManager.open(obj),0.5f);
            object3Ds = Loader.loadOBJ(assetManager.open(obj), assetManager.open(mtl), 1f);

/*
//            object3Ds = Loader.loadOBJ(assetManager.open("IronMan.obj"),assetManager.open("IronMan.mtl"),0.05f);
            if (currentTracker.equalsIgnoreCase("building"))
                object3Ds = Loader.loadOBJ(assetManager.open("unit2.obj"), assetManager.open("unit2.mtl"), 1f);
            else if (currentTracker.equalsIgnoreCase("floorplan_1"))
                object3Ds = Loader.loadOBJ(assetManager.open("floor1.obj"), assetManager.open("floor1.mtl"), 1f);
            else if (currentTracker.equalsIgnoreCase("floorplan_4"))                   // object3Ds = Loader.loadOBJ(objIS, mtlIS, 1f);
                object3Ds = Loader.loadOBJ(assetManager.open("floor4.obj"), assetManager.open("floor4.mtl"), 1f);
            else if (currentTracker.equalsIgnoreCase("floorplan_2"))
                object3Ds = Loader.loadOBJ(assetManager.open("floor2.obj"), assetManager.open("floor2.mtl"), 1f);
            else if (currentTracker.equalsIgnoreCase("floorplan_3"))
                object3Ds = Loader.loadOBJ(assetManager.open("floor3.obj"), assetManager.open("floor3.mtl"), 1f);
            objMerged = Object3D.mergeAll(object3Ds);
            //  objMerged.scale(40f);
            // Log.i("Object Loaded Size",);
            HashSet<String> hs = tm.getNames();
*/
            objMerged = Object3D.mergeAll(object3Ds);
            object3Ds = null;

            Set<String> set = tm.getNames();

//populate set

            for (String s : set) {
                //  File textureFile = getARObjectFile(s);
                //if (textureFile.exists()) {
                //   FileInputStream textureStream = new FileInputStream(textureFile);
                try {
                    Texture txt = new Texture(assetManager.open(s));
                    TextureManager.getInstance().replaceTexture(s, txt);
                }
                catch (FileNotFoundException ext)
                {}

                // }
            }
            //   MemoryHelper.compact();
      /*      String[] txtSets = (String[]) hs.toArray();
            for(int i=0 ; i< txtSets.length;i++)
            {
                File textureFile = getARObjectFile(txtSets[i]);
                if(textureFile.exists())
                {
                    FileInputStream textureStream = new FileInputStream(textureFile);
                    Texture txt = new Texture(textureStream);
                    TextureManager.getInstance().replaceTexture(txtSets[i], txt);
                }
            } */
            // objMerged.translate(x,y,z);
            // objMerged.rotateX(90f);
            objMerged.build();

            // if (currentTracker.equalsIgnoreCase("building")) {
            //   objMerged.rotateZ(90f);
            // objMerged.rotateY(180f);
            //   }


            SimpleVector a = objMerged.getCenter();
            //      SimpleVector b = objMerged.getOrigin();

            objMerged.translate(a.x * (-1f), a.y * (-1f), a.z * (-1f));
            objMerged.scale(1.5f);
            //    if (currentTracker.equalsIgnoreCase("building")) {
            //         objMerged.scale(0.3f);
            //objMerged.rotateY(180f);

            // SimpleVector y = objMerged.getCenter();
            //  objMerged.setRotationPivot(y);
            // objMerged.rotateY(3.14159f);
            //   objMerged.getRotationPivot()
            //               objMerged.rotateAxis(y,180f);
            //   a = objMerged.getTransformedCenter();
            //   objMerged.translate(a.x*(-1f),a.y*(-1f),a.z*(-1f));
               objMerged.rotateX(1.5708f);
                objMerged.rotateY(1.5708f);
            //     } else {
            //   SimpleVector y = objMerged.getCenter();
            //   objMerged.setRotationPivot(y);
            //

            //        }
            objMerged.rotateX(-1.5708f);
            cam = world.getCamera();
            cam.moveCamera(Camera.CAMERA_MOVEIN, 50);
            cam.lookAt(objMerged.getTransformedCenter());

            SimpleVector sv = new SimpleVector(objMerged.getTransformedCenter());
            sv.y += 10;
            sv.z += 50;
            sun.setPosition(sv);

            //      Log.i(a.toString(),b.toString());
            //  world.removeAllObjects();

            world.addObject(objMerged);
            // world.buildAllObjects();
            printObjLocation();
            objMerged = null;
            MemoryHelper.compact();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
       // hideLoader();
    }

    void printObjLocation() {
        Log.d("Printing 3D", "3D Value begin");
        Log.d("X Axis:", objMerged.getXAxis().toString());
        Log.d("Y Axis:", objMerged.getYAxis().toString());
        Log.d("Z Axis:", objMerged.getZAxis().toString());
        Log.d("Center:", objMerged.getCenter().toString());
        Log.d("Transformed Center:", objMerged.getTransformedCenter().toString());
        Log.d("Sun:", sun.getPosition().toString());
        Log.d("Origin:", objMerged.getOrigin().toString());
        // Log.d("Size:", getSize(objMerged).toString());
        Log.d("Printing 3D", "3D Value end");
    }

    void loadObjectToWorldAsync(final String currentTracker) {
        isLoadingModel = true;
        showLoader();
        new Thread(new Runnable() {
            public void run() {
                loadObjectToWorldSync(currentTracker);
                trackerStr = "pohan hendri shiela reynald siska andi";
                isLoadingModel = false;
                hideLoader();

            }
        }).start();
    }

    // Called to draw the current frame.
    @Override
    public void onDrawFrame(GL10 gl) {
        if (!mIsActive)
            return;

        // Call our function to render content
        renderFrame();
        updateCamera();
        //     if(shouldRender) {


        world.renderScene(fb);
        world.draw(fb);
        fb.display();
        //   }

    }


    // Called when the surface is created or recreated.
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Log.d(LOGTAG, "GLRenderer.onSurfaceCreated");


        initRendering(); // NOTE: Cocokin sama cpp - DONE
        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        vuforiaAppSession.onSurfaceCreated();
    }


    // Called when the surface changed size.
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.d(LOGTAG, "GLRenderer.onSurfaceChanged");

        if (fb != null) {
            fb.dispose();
        }
        fb = new FrameBuffer(width, height);
        Config.viewportOffsetAffectsRenderTarget = true;

        updateRendering(width, height);
        // Call Vuforia function to handle render surface size changes:
        vuforiaAppSession.onSurfaceChanged(width, height);
    }


    // Function for initializing the renderer.
    private void initRendering() {
        mRenderer = Renderer.getInstance();

        // Define clear color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f : 1.0f);

        // Hide the Loading Dialog
        //mActivity.loadingDialogHandler
        //       .sendEmptyMessage(LoadingDialogHandler.HIDE_LOADING_DIALOG);
    }

    private void updateRendering(int width, int height) {

        // Update screen dimensions
        vuforiaAppSession.setmScreenWidth(width);
        vuforiaAppSession.setmScreenHeight(height);

        // Reconfigure the video background
        vuforiaAppSession.configureVideoBackground();

        CameraCalibration camCalibration = com.vuforia.CameraDevice.getInstance().getCameraCalibration();
        Vec2F size = camCalibration.getSize();
        Vec2F focalLength = camCalibration.getFocalLength();
        float fovyRadians = (float) (2 * Math.atan(0.5f * size.getData()[1] / focalLength.getData()[1]));
        float fovRadians = (float) (2 * Math.atan(0.5f * size.getData()[0] / focalLength.getData()[0]));

        if (vuforiaAppSession.mIsPortrait) {
            setFovy(fovRadians);
            setFov(fovyRadians);
        } else {
            setFov(fovRadians);
            setFovy(fovyRadians);
        }

    }

    // The render function.
    private void renderFrame() {
        // clear color and depth buffer
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        // get the state, and mark the beginning of a rendering section
        State state = mRenderer.begin();
        // explicitly render the video background
        mRenderer.drawVideoBackground();

        float[] modelviewArray = new float[16];
        isPlayingVideo = false;
        // did we find any trackables this frame?
        for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
            //    if(isPlayingVideo)
            //       break;
            // get the trackable
            TrackableResult result = state.getTrackableResult(tIdx);
            Trackable trackable = result.getTrackable();

            loadObjectToWorld(trackable.getName());// Load 3D Object to screen


            Matrix44F modelViewMatrix = Tool.convertPose2GLMatrix(result.getPose());
            Matrix44F inverseMV = SampleMath.Matrix44FInverse(modelViewMatrix);
            Matrix44F invTranspMV = SampleMath.Matrix44FTranspose(inverseMV);

            modelviewArray = invTranspMV.getData();
            updateModelviewMatrix(modelviewArray);

            break;// Only one to process 1 target at a time
        }
        // hide the objects when the targets are not detected
        if (state.getNumTrackableResults() == 0) {
            float m[] = {
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, -10000, 1
            };
            modelviewArray = m;
            updateModelviewMatrix(modelviewArray);
        }
        mRenderer.end();
    }


    private void printUserData(Trackable trackable) {

        String userData = (String) trackable.getUserData();
        Log.d(LOGTAG, "UserData:Retreived User Data	\"" + userData + "\"");
    }

    private void updateModelviewMatrix(float mat[]) {
        modelViewMat = mat;
    }

    private void updateCamera() {
        if (modelViewMat != null) {
            float[] m = modelViewMat;

            final SimpleVector camUp;
            if (vuforiaAppSession.mIsPortrait) {
                camUp = new SimpleVector(-m[0], -m[1], -m[2]);
            } else {
                camUp = new SimpleVector(-m[4], -m[5], -m[6]);
            }

            final SimpleVector camDirection = new SimpleVector(m[8], m[9], m[10]);
            final SimpleVector camPosition = new SimpleVector(m[12], m[13], m[14]);

            cam.setOrientation(camDirection, camUp);
            cam.setPosition(camPosition);

            cam.setFOV(fov);
            cam.setYFOV(fovy);
        }
    }

    private void setFov(float fov) {
        this.fov = fov;
    }

    private void setFovy(float fovy) {
        this.fovy = fovy;
    }

    public void clearWorld()
    {
        /*if(world != null) {
            for (Enumeration<Object3D> e = world.getObjects(); e.hasMoreElements(); )
                e.nextElement().clearObject();
            world.removeAllObjects();
        }
        MemoryHelper.compact(); */
    }
}
